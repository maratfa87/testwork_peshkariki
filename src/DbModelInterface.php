<?php

/**
 * Base interface for database entities.
 */
interface DbModelInterface
{
    public static function findAll();
}
