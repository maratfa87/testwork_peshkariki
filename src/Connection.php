<?php

final class Connection
{
    private $pdo;
    private $dsn;
    private $username;
    private $password;
    private static $instance = null;

    private function __construct()
    {
        $this->configure();
        $this->pdo = new \PDO(
            $this->dsn,
            $this->username,
            $this->password,
            [
                \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES => true,
            ]
        );
        $this->pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
    }

    private function configure()
    {
        $config = require __DIR__ . '/config/db.php';
        $this->dsn = "mysql:host={$config['dbHost']};dbname={$config['dbName']};charset=utf8";
        $this->username = $config['dbUser'];
        $this->password = $config['dbPass'];
    }

    public static function getInstance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}
