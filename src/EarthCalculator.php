<?php

class EarthCalculator
{
    /**
     * Earth radius in kilometers
     */
    const EARTH_RADIUS = 6378;

    /**
     * Spherical Law of Cosines
     * @see https://www.movable-type.co.uk/scripts/latlong.html
     * @param GeoPoint $point1
     * @param GeoPoint $point2
     * @return float Distance between two points on Earth sphere in kilometers
     */
    public static function getDistance(GeoPoint $point1, GeoPoint $point2)
    {
        $latA = deg2rad(floatval($point1->latitude));
        $latB = deg2rad(floatval($point2->latitude));
        $lngA = deg2rad(floatval($point1->longitude));
        $lngB = deg2rad(floatval($point2->longitude));

        return self::EARTH_RADIUS * acos((sin($latA) * sin($latB)) + (cos($latA) * cos($latB) * cos($lngB - $lngA)));
    }
}
