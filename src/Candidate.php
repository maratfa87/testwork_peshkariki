<?php

class Candidate extends CandidateAbstract
{
    public function run()
    {
        if (($_SERVER['REQUEST_METHOD'] === 'POST') && ($_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest')) {
            $userName = $this->getPostParam('name');
            $userCoords = Toolkit::getCoords($this->getPostParam('address'));
            $userPhone = Toolkit::getFormattedPhone($this->getPostParam('phoneNumber'));

            $results = $this->calculateDistance($userCoords);
            usort($results, function ($a, $b) {
                return $a['distance'] <=> $b['distance'];
            });

            $closestStore = '';
            $closestDistance = '';
            foreach ($results as $item) {
                $closestStore = $item['name'];
                $closestDistance = $item['distance'];
                break;
            }
            print("[$userName] ([$userPhone]): ближайший пункт выдачи [$closestStore] находится на расстоянии [$closestDistance]км");
        } else {
            header("HTTP/1.0 405 Method Not Allowed");
            exit();
        }
    }

    private function getPostParam($param)
    {
        $data = trim($_POST[$param]);
        $data = htmlspecialchars($data);
        $data = strip_tags($data);
        $data = stripslashes($data);

        return $data;
    }

    /**
     * @param $coordsCandidate array
     * @return array
     */
    protected function calculateDistance($coordsCandidate)
    {
        $storeLocations = GeoPoint::findAll();
        $userLocation = new GeoPoint(
            $this->getPostParam('name'),
            (string)$coordsCandidate['lat'],
            (string)$coordsCandidate['lng']
        );

        foreach ($storeLocations as $storeLocation) {
            $distance = round(EarthCalculator::getDistance($userLocation, $storeLocation), 1);
            $distances[] = [
                'name' => $storeLocation->name,
                'distance' => $distance,
            ];
        }

        return $distances;
    }
}
