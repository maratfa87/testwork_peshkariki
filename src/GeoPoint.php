<?php

class GeoPoint implements DbModelInterface
{
    /** @var string */
    public $name;
    /** @var string */
    public $latitude;
    /** @var string */
    public $longitude;

    /**
     * GeoPoint constructor.
     * @param string $name
     * @param string $latitude
     * @param string $longitude
     */
    public function __construct($name, $latitude, $longitude)
    {
        $this->name = $name;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
    }

    /**
     * @return GeoPoint[]
     */
    public static function findAll()
    {
        $connection = Connection::getInstance();
        $statement = $connection->getPdo()->prepare('SELECT * FROM point');
        $statement->execute();

        $points = [];
        while ($resultRow = $statement->fetchObject()) {
            $points[] = new self($resultRow->name, $resultRow->latitude, $resultRow->longitude);
        }

        return $points;
    }
}
