<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>test work</title>
    <link href="src/styles/main.css" rel="stylesheet" type="text/css">
    <script src="src/js/script.js" type="application/javascript"></script>
</head>
<body>
    <form id="person-form"  onsubmit="sendData();return false">
        <label>ФИО<br>
            <input
                    id="name"
                    type="text"
                    pattern="[A-Za-zА-Яа-яЁё]{2,20}[\s][A-Za-zА-Яа-яЁё]{2,20}[\s][A-Za-zА-Яа-яЁё]{2,20}"
                    title="ФИО разделённое пробелами"
                    name="name"
                    placeholder="Иванов Иван Иванович"
                    required
            ><br>
        </label>
        <label>Телефон<br>
            <input
                    id="phoneNumber"
                    type="tel"
                    pattern="\d{10}"
                    title="10 цифр телефона без разделителей"
                    name="phoneNumber"
                    placeholder="1234567890"
                    required
            ><br>
        </label>
        <label>Адрес<br>
            <input
                    id="address"
                    type="text"
                    pattern=".{1,200}"
                    title="Адрес до 200 символов"
                    name="address"
                    placeholder="ул. Севастьянова, 15, Санкт-Петербург"
                    required
            ><br>
        </label>
        <button type="submit">Найти пункт выдачи</button>
    </form>
    <div id="form-results" class="hidden">
    </div>
</body>
</html>
