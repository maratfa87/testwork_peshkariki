function sendData() {
    var formElement = document.querySelector('#person-form');
    var formData = new FormData(formElement);
    var request = new XMLHttpRequest();
    request.open('POST', '/ajax.php');
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    request.onload = function() {
        if (request.status === 200) {
            var resultDiv = document.querySelector('#form-results');
            resultDiv.className = "visible";
            resultDiv.innerHTML = '<p>'+request.responseText+'</p>';
        } else {
            console.error('Request failed! Status is ' + request.status);
        }
    };
    request.send(formData);
}
